# Hyperios Programming


![bannière](.ressources/logo-hyperios500x500.png)

##  1. <a name='Tabledesmatires'></a> Table des matières
<!-- vscode-markdown-toc -->
* 1. [ Table des matières](#Tabledesmatieres)
* 2. [Ce qui est en place sur GitLab ?](#Cequiestenplace) 🦊🦊🦊
	* 2.1. [Les labels](#Leslabels)
	* 2.2. [Le Board](#LeBoard)
	* 2.3. [Les branches](#Lesbranches)
* 3. [Ce qu'on utilise dans le projet ?](#Cequonutilise)
* 4. [Utiliser ce dépot](#UtiliserCeDepot)
	* 4.1. Télecharger l'export du dépot
	* 4.2. Importer dans GitLab
* 5. [Licence](#Licence)
* 6. [Auteur](#Auteur)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  2. <a name='Cequiestenplace'></a>Ce qui est en place sur GitLab?

-   Ce fichier README.md
-   Des modèles pour les issues et les merges requests
-   Une collection de labels
-   Un modèle de Board
-   3 Branches :
    -   Main
    -   Pré-Production
    -   Production

###  2.1. <a name='Leslabels'></a>Les labels

Les labels sont des éléments qui sont associés à des issues et merges requests et qui permettent de les classer, le organiser et de les identifier simplement.

![label](.ressources/label.gif)

###  2.2. <a name='LeBoard'></a>Le Board

Le Board est l'outil central de GitLab pour organiser et gérer le projet.

Il va vous permettre de visualiser les différentes tâches que vous avez à accomplir, et de suivre leur progression.

La structure de ce board adopte l'approche [Scrumban](https://asana.com/fr/resources/scrumban).

![board](.ressources/liste-ticket.gif)

###  2.3. <a name='Lesbranches'></a>Les branches

Les trois branches proposées dans ce template permettent de gérer de manière simple l'état du déploiement du projet.

Ce modèle s'inspire librement de l'approche [GitLab Flow](https://www.youtube.com/watch?v=ZJuUz5jWb44).

![](.ressources/readme-3-branches.png)

##  3. <a name='Cequonutilise'></a>Ce qu'on utilise dans le projet ?

- !!! à remplir au fur et à mesure

##  4. <a name='UtiliserCeDepot'></a>Utiliser ce dépot

###  4.1.  [Télécharger l'export du dépot](.ressources/starter-pack-project.tar.gz)

###  4.2. [Importer dans GitLab](https://docs.gitlab.com/ee/user/project/settings/import_export.html#import-a-project-and-its-data)

##  5. <a name='Licence'></a>Licence

Ce dépôt est sous licence [MIT](LICENSE)

##  6. <a name='Auteur'></a>Auteur

[HyperiosProgramming](https://hyperios-programming.com/)